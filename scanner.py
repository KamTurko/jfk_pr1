import collections
import re

Token = collections.namedtuple('Token', ['type', 'value', 'line', 'column'])

class Scanner:

  def __init__(self, input):

    #clean comments
    input = re.sub(r'(\#.+)', '', input)

    print ("Input with removed comments:")
    print (input)

    self.tokens = []
    self.current_token_number = 0
    for token in self.tokenize(input):
      self.tokens.append(token)
 
  def tokenize(self, input_string):
    keywords = {'begin', 'end', 'gnd', 'voltagesource', 'voltageprobe', 'currentsource', 'currentprobe', 'resistor', 
      'capacitor', 'inductor', 'diode', 'EOF'}
    token_specification = [
        ('SCI',     r'-?([1-9]\d*)(?:\.\d+)?(?:[eE][+\-]?\d+)'), #scientific notation
        ('FLOAT',   r'\d+(\.\d*)'),        # Float
        ('ID',      r'[A-Za-z_]+[0-9]*'),    # Identifiers
        ('INT',     r'[0-9]+'),             # Integer
        ('ASSIGN',  r'='),                  # Assignment operator
        ('EOL',     r'\n'),                 # Line endings
        ('SKIP',    r'[ \t]'),              # Skip over spaces and tabs
        ("L_BRA",   r'\('),                 # left bracket
        ("R_BRA",   r'\)'),                 # right bracket
        ("L_S_BRA", r'\['),                 # left square bracket
        ("R_S_BRA", r'\]'),                 # right square bracket
        ("COMMA",   r','),                  # comma for separating parameters
        ("CONNECT", r'--'),                 # double dash for net connections
    ]
    tok_regex = '|'.join('(?P<%s>%s)' % pair for pair in token_specification)
    get_token = re.compile(tok_regex).match
    line_number = 1
    current_position = line_start = 0
    match = get_token(input_string)
    while match is not None:
        type = match.lastgroup
        if type == 'EOL':
            line_start = current_position
            line_number += 1
        elif type != 'SKIP':
            value = match.group(type)
            if type == 'ID' and value in keywords:
                type = value
            yield Token(type, value, line_number, match.start()-line_start)
        current_position = match.end()
        match = get_token(input_string, current_position)
    if current_position != len(input_string):
        raise RuntimeError('Error: Unexpected character %r on line %d' % \
                              (input_string[current_position], line_number))
    yield Token('EOF', '', line_number, current_position-line_start)

  def next_token(self):
    self.current_token_number += 1
    if self.current_token_number-1 < len(self.tokens):
      return self.tokens[self.current_token_number-1]
    else:
      raise RuntimeError('Error: No more tokens')

