
class Parser:

  ##### Parser header #####
  def __init__(self, scanner):
    self.next_token = scanner.next_token
    self.token = self.next_token()

  def take_token(self, token_type):
    if self.token.type != token_type:
      msg = f"Expected token: {token_type} (given token = {self.token.type} at line={self.token.line}, column={self.token.column} )"
      self.error(msg)
    if token_type != 'EOF':
      self.token = self.next_token()
      #print("Token: " + token_type + " taken successfully")

  def error(self,msg):
    raise RuntimeError('Parser error, %s' % msg)

  ##### Parser body #####

############################################################################

  # Starting symbol
  def start(self):
    # program -> 'begin' commands 'end'
    if self.token.type == 'begin' or self.token.type == 'end':
      self.take_token('begin')
      self.commands()
      self.take_token('end')
      print("Success! Code is flawless")
    else:
      self.error("Epsilon not allowed")

  def commands(self):
    # print ("Commands!")
    # commands -> command 'eol' commands
    if self.token.type == 'ID' or self.token.type == 'gnd':
      self.command()
      #self.take_token('EOL')
      self.commands()
    # commands -> eps
    else:
      pass

  def command(self):
    # print ("Command!")
    if self.token.type == 'ID':
      self.take_token("ID")
      #command -> connections
      if self.token.type == 'L_S_BRA':
        self.connections()
      #command -> assignment
      elif self.token.type == 'ASSIGN':
        self.assignment()
      else:
        self.error("Epsilon not allowed") 
    elif self.token.type == 'gnd':
      self.connections()
    else:
      self.error("Epsilon not allowed")

  def connections(self):
    # print ("Connections!")
    # connections -> pin connection_next (pin ma już zabrane ID)
    if self.token.type == 'L_S_BRA':
      self.pin()
      self.connection_next()
    elif self.token.type == 'gnd':
      self.pin()
      self.connection_next()
    # connections -> eps
    else:
      pass

  def connection_next(self):
    # print ("Connection_next!")
    # print("entering connection_next with: ", self.token
    # connection_next -> '--' pin connection_next
    if self.token.type == 'CONNECT':
      self.take_token("CONNECT")
      self.pin()
      self.connection_next()
    # connection_next -> eps
    else:
      pass

  def pin(self):
    # print ("Pin!")
    # pin -> ID '[' INT ']' (ale z już zabranym ID)
    if self.token.type == 'L_S_BRA':
      #self.take_token("ID")
      self.take_token("L_S_BRA")
      self.take_token("INT")
      self.take_token("R_S_BRA")
    # pin -> ID '[' INT ']'
    elif self.token.type == 'ID':
      self.take_token("ID")
      self.take_token("L_S_BRA")
      self.take_token("INT")
      self.take_token("R_S_BRA")
    #pin -> "gnd"
    elif self.token.type == 'gnd':
      self.take_token("gnd")
    else:
      self.error("Epsilon not allowed")

  def assignment(self):
    # print ("Assignment!")
    # assignment -> ID '=' type parameters (ID ma już zabrane przez command())
    if self.token.type == 'ASSIGN':
      #self.take_token("ID")
      self.take_token("ASSIGN")
      self.type()
      self.parameters()
    else:
      self.error("Epsilon not allowed")

  def param_assignment(self):
    # print ("Param_assignment!")
    # param_assignment -> ID '=' value
    if self.token.type == 'ID':
      self.take_token("ID")
      self.take_token("ASSIGN")
      self.value()
    else:
      self.error("Epsilon not allowed")

  def parameters(self):
    # print ("Parameters!")
    # parameters -> '(' parameters_count ')'
    if self.token.type == 'L_BRA':
      self.take_token("L_BRA")
      self.parameters_count()
      self.take_token("R_BRA")
    else:
      self.error("Epsilon not allowed")

  def parameters_count(self):
    # print ("Parameters_count!")
    if self.token.type == 'ID':
      self.take_token("ID")
      # parameters_count -> value parameters_next
      if self.token.type == 'ASSIGN':
        self.take_token("ASSIGN")
        self.value()
      # parameters_count -> param_assignment parameters_next
      else:
        self.param_assignment()
      self.parameters_next()
    # parameters_count -> eps
    elif self.token.type in ["INT","FLOAT","SCI"]:
      self.value()
    else:
      pass

  def parameters_next(self):
    # print ("Parameters_next!")
    # parameters_next -> ',' parameters_count
    if self.token.type == 'COMMA':
      self.take_token("COMMA")
      self.parameters_count()
    # parameters_next -> eps
    else:
      pass

  def value(self):
    # print ("Value!")
    # value -> INT
    if self.token.type == 'INT':
      self.take_token("INT")
    # value -> FLOAT
    elif self.token.type == 'FLOAT':
      self.take_token("FLOAT")
    # value -> SCIENTIFIC
    elif self.token.type == 'SCI':
      self.take_token("SCI")
    # value -> FLOAT
    elif self.token.type == 'ID':
      self.take_token("ID")
    else:
      self.error("Epsilon not allowed")

  def type(self):
    # print ("Type!")
    # type -> voltagesource
    if self.token.type == 'voltagesource':
      self.take_token("voltagesource")
    # type -> voltageprobe
    elif self.token.type == 'voltageprobe':
      self.take_token("voltageprobe")
    # type -> currentsource
    elif self.token.type == 'currentsource':
      self.take_token("currentsource")
    # type -> currentprobe
    elif self.token.type == 'currentprobe':
      self.take_token("currentprobe")
    # type -> resistor
    elif self.token.type == 'resistor':
      self.take_token("resistor")
    # type -> capacitor
    elif self.token.type == 'capacitor':
      self.take_token("capacitor")
    # type -> inductor
    elif self.token.type == 'inductor':
      self.take_token("inductor")
    # type -> diode
    elif self.token.type == 'diode':
      self.take_token("diode")
    else:
      self.error("Epsilon not allowed")