# Simple example of parsing
# Bartosz Sawicki, 2014-03-13

from scanner import *
from _parser import Parser

#input_string = '''
#x := 5;
#y := x;
#PRINT 64;
#'''

input_string = '''
begin
  R1 = resistor(13.2) # rezystor o wartości 13.2 ohm
  C1 = capacitor(100e-9) # kondensator o wartości 100e-9 faradów
  VIN = voltagesource()
  AM1 = currentprobe()

  VIN[2] -- R1[1]
  R1[2] -- C1[1]
  C1[2] -- AM1[1]
  AM1[2] -- VIN[1]
  VIN[1] -- gnd
end
'''

print (input_string)
scanner = Scanner(input_string)
#print (scanner.tokens)

parser = Parser(scanner)
parser.start()
  
